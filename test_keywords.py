import unittest
import keywords

class TestKeywords(unittest.TestCase):
    def test_parse_files(self):
        kw = keywords.parse_files('tests/parse_basic_test')
        self.assertIn('topic 1', kw)
        self.assertIn('topic 2', kw)

        self.assertIn('keywords aaaa', kw['topic 1'])
        self.assertIn('keywords bbbbbbb', kw['topic 1'])
        self.assertIn('lfh\';e29312', kw['topic 1'])
        self.assertIn('3938904;\'\'/][/\'', kw['topic 2'])
        self.assertIn('-=\][,./\';!@!)(#&*%^$&*(^&)#$', kw['topic 2'])

    def test_parse_empty_file(self):
        k = keywords.parse_files('tests/empty')
        self.assertEqual(k, {})

    def test_parse_some_empty_keywords(self):
        k = keywords.parse_files('tests/some_empty')
        self.assertNotIn('topic2', k)

    def test_parse_garbage_input(self):
        #print(keywords.parse_files('tests/random'))
        print('test_parse_garbage_input not implemented yet')

if __name__ == '__main__':
    unittest.main()
